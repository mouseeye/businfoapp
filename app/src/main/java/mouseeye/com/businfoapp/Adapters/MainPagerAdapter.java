package mouseeye.com.businfoapp.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by Arron on 2015-11-05.
 */
public class MainPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<String> tabNames = null;
    private ArrayList<Fragment> fragments = null;

    public MainPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        if(tabNames.size() > 0 && fragments.size() > 0){
            return tabNames.size();
        }
        return 0;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabNames.get(position);
    }

    public void addInitData(ArrayList<Fragment> fragments, ArrayList<String> tabNames) throws Exception {
        if(fragments.size() != tabNames.size()){
            throw new Exception("invalid size of list");
        }
        this.fragments = fragments;
        this.tabNames = tabNames;
    }
}