package mouseeye.com.businfoapp;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;
import mouseeye.com.businfoapp.adapters.MainPagerAdapter;
import mouseeye.com.businfoapp.views.fragments.BookMarkFragment;
import mouseeye.com.businfoapp.views.fragments.BusFragment;
import mouseeye.com.businfoapp.views.fragments.StationFragment;

public class MainActivity extends AppCompatActivity {
    TabLayout tabLayout;
    ViewPager viewPager;
    MainPagerAdapter mainPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.viewPager);

        mainPagerAdapter = new MainPagerAdapter(getSupportFragmentManager());
        initMainPagerAdapter();
        tabLayout.setTabsFromPagerAdapter(mainPagerAdapter);
        viewPager.setAdapter(mainPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
    }

    private void initMainPagerAdapter() {
        String[] strTab = getResources().getStringArray(R.array.tab_names);
        ArrayList<String> tabNames = new ArrayList<>();
        ArrayList<Fragment> tabFragments = new ArrayList<>();

        for (String name: strTab) {
            tabNames.add(name);
        }

        tabFragments.add(BookMarkFragment.newInstance());
        tabFragments.add(BusFragment.newInstance());
        tabFragments.add(StationFragment.newInstance());

        try {
            mainPagerAdapter.addInitData(tabFragments, tabNames);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }
}
